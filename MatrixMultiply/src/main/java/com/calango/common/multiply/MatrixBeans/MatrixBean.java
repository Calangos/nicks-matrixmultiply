package com.calango.common.multiply.MatrixBeans;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Calango
 */
public class MatrixBean {
    private List<int[]> matrix1 = new ArrayList<>();
    private List<int[]> matrix2 = new ArrayList<>();

    public List<int[]> getMatrix1() {
        return matrix1;
    }

    public void setMatrix1(int[] matrix1String) {
        this.matrix1.add(matrix1String);
    }

    public List<int[]> getMatrix2() {
        return matrix2;
    }

    public void setMatrix2(int[] matrix2String) {
        this.matrix2.add(matrix2String);
    }
}
