package com.calango.common.multiply;

import com.calango.common.multiply.MatrixBeans.MatrixBean;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 *
 * @author Calango
 */
public class MatrixReader {
    public MatrixBean readFromTextArea(
            HttpServletRequest request, 
            HttpServletResponse response)
                throws ServletException, IOException{
        MatrixBean matrixBean = new MatrixBean();
        String matrixLines1, matrixLines2;
        int j;
        matrixLines1 = request.getParameter("matrix1");
        matrixLines2 = request.getParameter("matrix2");
        /**
         * If one of the matrix or both of them are empty:  
         *  the program return the error massage to the matrix page 
         *  and show it to the user.
         */
        if ((matrixLines1.isEmpty())&&(matrixLines2.isEmpty())){
            request.setAttribute("error", "Matrix 1 AND Matrix 2 are empty.");
            request.getServletContext().getRequestDispatcher("/welcome/matrix").forward(request, response);
            return null;
        }
        if (matrixLines1.isEmpty()){
            request.setAttribute("error", "Matrix 1 is empty.");
            request.getServletContext().getRequestDispatcher("/welcome/matrix").forward(request, response);
            return null;
        }
        if (matrixLines2.isEmpty()){
            request.setAttribute("error", "Matrix 2 is empty.");
            request.getServletContext().getRequestDispatcher("/welcome/matrix").forward(request, response);
            return null;
        }    
        /**
         * If both matrixs are not empty:
         *  the program reads and converts to int them from the text-area
         */
        // Reading the first block
        try{
        BufferedReader reader = new BufferedReader(new StringReader(matrixLines1));
        String[] spiltLine;
        int[] matrixString;
        for (String line; (line = reader.readLine()) != null;) {
            j = 0;
            spiltLine = line.split(" ");
            matrixString = new int[spiltLine.length];
            for (String s: spiltLine)
                if(!("".equals(s)))
                    matrixString[j++] = Integer.parseInt(s);
            matrixBean.setMatrix1(matrixString);
        }
        reader.close();
        //Reading the second block
        reader = new BufferedReader(new StringReader(matrixLines2));    
        for (String line; (line = reader.readLine()) != null;) {
            j = 0;
            spiltLine = line.split(" ");
            matrixString = new int[spiltLine.length];
            for (String s: spiltLine)
                if(!("".equals(s)))
                    matrixString[j++] = Integer.parseInt(s);
            matrixBean.setMatrix2(matrixString);
        }    
        reader.close(); 
        } catch (NumberFormatException ex){
            request.setAttribute("error", "Invalid input of data.");
            request.getServletContext().getRequestDispatcher("/welcome/matrix").forward(request, response);
            return null;
        }
        /**
         * The program returns bean if the reading has been succeeded
         */
        return matrixBean;
    }    
}
