package com.calango.common.multiply.MatrixBeans;

/**
 *
 * @author Calango
 */
public class MatrixResult {
    private int[][] matrixResult;

    public void createMatrixResult(int length){
        matrixResult = new int[length][length];
    }
    
    public int[][] getMatrixResult() {
        return matrixResult;
    }

    public void setMatrixResult(int i, int j, int value) {
        this.matrixResult[i][j] = value;
    }
}
