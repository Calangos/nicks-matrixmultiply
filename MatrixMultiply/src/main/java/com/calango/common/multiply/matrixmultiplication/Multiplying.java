package com.calango.common.multiply.matrixmultiplication;
import com.calango.common.multiply.MatrixBeans.MatrixBean;
import com.calango.common.multiply.MatrixBeans.MatrixResult;

/**
 *
 * @author Calango
 */
public class Multiplying {
    public void multyply(int row, int column, MatrixBean matrixBean, MatrixResult mxRes){
        int i;
        int result = 0;
        for (i = 0; i < matrixBean.getMatrix1().size(); i++){
            result += 
                matrixBean.getMatrix1().get(row)[i] * 
                    matrixBean.getMatrix2().get(i)[column];
        }
        //System.out.println(result);
        mxRes.setMatrixResult(row, column, result);
        //MatrixStorage.setMatrixResult(row, column, result);
    }
}
