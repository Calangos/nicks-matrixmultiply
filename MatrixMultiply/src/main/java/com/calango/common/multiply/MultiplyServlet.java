package com.calango.common.multiply;

import com.calango.common.multiply.MatrixBeans.GeneralBean;
import com.calango.common.multiply.MatrixBeans.MatrixBean;
import com.calango.common.multiply.MatrixBeans.MatrixResult;
import com.calango.common.multiply.matrixmultiplication.MultiplicationImplementation;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class MultiplyServlet extends HttpServlet {
    public MultiplyServlet() {
        super();
    }

    @Override
    public void init() throws ServletException {
    }

    @Override
    protected void doGet(
            HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        process(request, response);
    }

    @Override
    protected void doPost(
            HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        process(request, response);
    }

    private void process(
            HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException{
        MatrixResult result;
        MatrixBean matrixBean;
        GeneralBean genBean = new GeneralBean();
        MatrixReader mxReader = new MatrixReader();
        InsertRecord insertRecord = new InsertRecord();
        MultiplicationImplementation multiplyCentre = new MultiplicationImplementation();
        response.setContentType("text/html");
        genBean.setMatrixName(request.getParameter("matrix_name"));
        if ("".equals(genBean.getMatrixName()))
            genBean.setMatrixName("Default Name");
        matrixBean = mxReader.readFromTextArea(request, response);
        if (matrixBean != null){
            if (
                (matrixBean.getMatrix1().size() != matrixBean.getMatrix1().get(0).length) &&
                    (matrixBean.getMatrix2().size() != matrixBean.getMatrix2().get(0).length) &&
                        (matrixBean.getMatrix1().size() != matrixBean.getMatrix2().size())
                ){
                request.setAttribute("error", "Matrixes are not square!");
                request.getServletContext().getRequestDispatcher("/welcome/matrix").forward(request, response);
                return;
            }
            genBean.setMatrixLength(matrixBean.getMatrix1().size());
            result = multiplyCentre.multiply(matrixBean);
            insertRecord.insert(genBean, matrixBean, result);
            request.setAttribute("matrixName", genBean.getMatrixName());
            request.setAttribute("matrixResult", result.getMatrixResult());
            request.getServletContext().getRequestDispatcher("/answer.jsp").forward(request, response);
        }
    }
    
    @Override
    public void destroy() {
        super.destroy();
    }
}