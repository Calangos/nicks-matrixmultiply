package com.calango.common.multiply;


import com.calango.common.multiply.MatrixBeans.GeneralBean;
import com.calango.common.multiply.MatrixBeans.MatrixBean;
import com.calango.common.multiply.MatrixBeans.MatrixResult;
import com.calango.daolayer.dao.impl.JdbcElementsDAO;
import com.calango.daolayer.dao.impl.JdbcHistoryDAO;
import com.calango.daolayer.dao.impl.JdbcMatrixDAO;
import com.calango.daolayer.model.Elements;
import com.calango.daolayer.model.History;
import com.calango.daolayer.model.Matrix;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Calango
 */
public class InsertRecord {

    public void insert(GeneralBean genBean, MatrixBean mxBean, MatrixResult result) {
                ApplicationContext context = 
    		new ClassPathXmlApplicationContext("spring-database.xml");
        JdbcHistoryDAO historyDAO = (JdbcHistoryDAO) context.getBean("historyDAO");
        JdbcMatrixDAO matrixDAO = (JdbcMatrixDAO) context.getBean("matrixDAO");
        JdbcElementsDAO elementsDAO = (JdbcElementsDAO) context.getBean("elementsDAO");
        Matrix matrix;
        Elements elements;
        History history;
        int i,j;
        //insert matrix 1
        matrix = new Matrix(
                1,
                1,
                genBean.getMatrixName(),
                mxBean.getMatrix1().size()
        );
        matrixDAO.insertRecord(matrix);
        //getting max matrix ID
        int maxID = matrixDAO.findIdByName(genBean.getMatrixName());
        //insert matrix 2
        matrixDAO.insertRecord(matrix);
        //insert matrix 3
        matrixDAO.insertRecord(matrix);
        //Insert Element1
        for (i = 0; i < mxBean.getMatrix1().size(); i++)
                for (j = 0; j < mxBean.getMatrix1().size(); j++){
                    elements = new Elements(
                            1,
                            i,
                            j,
                            mxBean.getMatrix1().get(i)[j],
                            maxID
                    );
                    elementsDAO.insert(elements);
                }
                //Insert Element2
        for (i = 0; i < mxBean.getMatrix2().size(); i++)
                for (j = 0; j < mxBean.getMatrix2().size(); j++){
                    elements = new Elements(
                            1,
                            i,
                            j,
                            mxBean.getMatrix2().get(i)[j],
                            maxID+1
                    );
                    elementsDAO.insert(elements);
                }
                        //Insert Element3
        for (i = 0; i < result.getMatrixResult().length; i++)
                for (j = 0; j < result.getMatrixResult().length; j++){
                    elements = new Elements(
                            1,
                            i,
                            j,
                            result.getMatrixResult()[i][j],
                            maxID+2
                    );
                    elementsDAO.insert(elements);
                }
        history = new History(0, maxID, maxID + 1, maxID + 2, null);
        historyDAO.insertHistory(history);   
    }
}