package com.calango.common.multiply.matrixmultiplication;

import com.calango.common.multiply.MatrixBeans.MatrixBean;
import com.calango.common.multiply.MatrixBeans.MatrixResult;

/**
 *
 * @author Calango
 */
public class MultiplicationImplementation {
    public final int nThreads = 5;
    public MatrixResult multiply(MatrixBean matrixBean){
        MatrixResult result = new MatrixResult();
        WorkQueue workQueue = new WorkQueue(nThreads);
        int i,j;
        result.createMatrixResult(matrixBean.getMatrix1().size());
        for (i = 0; i < matrixBean.getMatrix1().size(); i++)
            for (j = 0; j < matrixBean.getMatrix1().size(); j++){
                ThreadMultiply multiplyMission = new ThreadMultiply();
                multiplyMission.setWork(i, j, matrixBean, result);
                workQueue.execute(multiplyMission); 
            }
        while (!workQueue.isAllWorkDone()) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return result;
    }    
}
