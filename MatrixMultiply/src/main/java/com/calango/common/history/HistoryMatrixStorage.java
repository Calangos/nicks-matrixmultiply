package com.calango.common.history;

/**
 *
 * @author Calango
 */
public class HistoryMatrixStorage {
    private String matrixName = null;
    private int[][] matrix1 = null; 
    private int[][] matrix2 = null; 
    private int[][] matrixResult = null;
    
    HistoryMatrixStorage(String matrixName, int[][] matrix1, int[][] matrix2, int[][] matrixResult){
        this.matrixName = matrixName;
        this.matrix1 = matrix1;
        this.matrix2 = matrix2;
        this.matrixResult = matrixResult;
    }
            
    public String getMatrixName() {
        return matrixName;
    }

    public void setMatrixName(String matrixName) {
        this.matrixName = matrixName;
    }

    public int[][] getMatrix1() {
        return matrix1;
    }

    public void setMatrix1(int[][] matrix1) {
        this.matrix1 = matrix1;
    }

    public int[][] getMatrix2() {
        return matrix2;
    }

    public void setMatrix2(int[][] matrix2) {
        this.matrix2 = matrix2;
    }

    public int[][] getMatrixResult() {
        return matrixResult;
    }

    public void setMatrixResult(int[][] matrixResult) {
        this.matrixResult = matrixResult;
    }
}
