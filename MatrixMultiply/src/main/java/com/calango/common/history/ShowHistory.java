package com.calango.common.history;

import com.calango.daolayer.dao.impl.JdbcElementsDAO;
import com.calango.daolayer.dao.impl.JdbcHistoryDAO;
import com.calango.daolayer.dao.impl.JdbcMatrixDAO;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class ShowHistory{
    private final int USER_ID = 1;
    private String matrixName = null;
    private int[][] matrix1 = null; 
    private int[][] matrix2 = null; 
    private int[][] matrixResult = null;
    
    public HistoryMatrixStorage historyUpdate(String selectedDate){
        ApplicationContext context = 
    		new ClassPathXmlApplicationContext("spring-database.xml");
        JdbcHistoryDAO historyDAO = (JdbcHistoryDAO) context.getBean("historyDAO");
        JdbcMatrixDAO matrixDAO = (JdbcMatrixDAO) context.getBean("matrixDAO");
        JdbcElementsDAO elementsDAO = (JdbcElementsDAO) context.getBean("elementsDAO");
        int i,j;
        int matrixID = historyDAO.findIdByDate(selectedDate);

        matrixName = matrixDAO.findMatrixNameById(matrixID);
        int matrixLength = matrixDAO.getMatrixLength(matrixID);
        matrix1 = new int[matrixLength][matrixLength];
        matrix2 = new int[matrixLength][matrixLength];  
        
        //Insert Matrix1
        for (i = 0; i < matrixLength; i++)
            for (j = 0; j < matrixLength; j++)
                matrix1[i][j] = elementsDAO.getValue(matrixID, i, j);
       //Insert Matrix2               
        matrixID++;
        for (i = 0; i < matrixLength; i++)
            for (j = 0; j < matrixLength; j++)
                matrix2[i][j] = elementsDAO.getValue(matrixID, i, j);
        //Insert Matrix3
        matrixResult = new int[matrixLength][matrixLength];
        matrixID++;
        for (i = 0; i < matrixLength; i++)
            for (j = 0; j < matrixLength; j++)            
                matrixResult[i][j] = elementsDAO.getValue(matrixID, i, j);
        HistoryMatrixStorage storage = new HistoryMatrixStorage(
            matrixName,matrix1,matrix2,matrixResult
        );
        return storage;
    }       
}