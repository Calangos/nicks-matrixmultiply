package com.calango.common.history;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class HistoryServlet extends HttpServlet {
    
    public HistoryServlet() {
        super();
    }

    @Override
    public void init() throws ServletException {
    }

    @Override
    protected void doGet(
            HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        processing(request, response);
    }

    @Override
    protected void doPost(
            HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        processing(request, response);
    }
    
    private void processing(
            HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException{
        String selectedDate = request.getParameter("date");
        if (selectedDate == null){
            GetDatesList getterDates = new GetDatesList();
            List<String> dates = getterDates.getDatesList();
            request.setAttribute("datesList", dates);
            request.getRequestDispatcher("ShowHistory.jsp").forward(request, response);
        }
        else {
            ShowHistory update = new ShowHistory();
            HistoryMatrixStorage storage = update.historyUpdate(selectedDate);
            request.setAttribute("matrixName", storage.getMatrixName());
            request.setAttribute("matrix1", storage.getMatrix1());
            request.setAttribute("matrix2", storage.getMatrix2());
            request.setAttribute("matrixResult", storage.getMatrixResult());
            request.getRequestDispatcher("history.jsp").forward(request, response);
        }
    }
    
    @Override
    public void destroy() {
        super.destroy();
    }
}