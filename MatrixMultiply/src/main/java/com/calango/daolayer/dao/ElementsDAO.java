package com.calango.daolayer.dao;

import com.calango.daolayer.model.Elements;

/**
 *
 * @author Calango
 */
public interface ElementsDAO {
    public Object findByID(int id);
    public int getValue(int matrixID, int rowID, int columnID);
    public void insert(Elements insertObject);
    
}
