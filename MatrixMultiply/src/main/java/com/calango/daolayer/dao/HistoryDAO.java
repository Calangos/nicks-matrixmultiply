package com.calango.daolayer.dao;

import com.calango.daolayer.model.History;
import java.util.List;

/**
 *
 * @author Calango
 */
public interface HistoryDAO {
    public History findByID(int id);
    public int findIdByDate(String date);
    public List<String> getDatesList();
    public void insertHistory(History insertObject);
}