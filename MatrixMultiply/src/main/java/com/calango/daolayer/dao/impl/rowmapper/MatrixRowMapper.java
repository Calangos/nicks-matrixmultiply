package com.calango.daolayer.dao.impl.rowmapper;

import com.calango.daolayer.model.Matrix;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author Calango
 */
public class MatrixRowMapper implements RowMapper{

    @Override
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        Matrix matrix = new Matrix();
        matrix.setMatrixId(rs.getInt("matrix_id"));
        matrix.setUserId(rs.getInt("user_id"));
        matrix.setMatrixName(rs.getString("matrix_name"));
        matrix.setMatrixLength(rs.getInt("matrix_length"));
        return matrix;
    }
    
}
