package com.calango.daolayer.dao.impl;

import com.calango.daolayer.dao.MatrixDAO;
import com.calango.daolayer.dao.impl.rowmapper.MatrixRowMapper;
import com.calango.daolayer.model.Matrix;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author Calango
 */
public class JdbcMatrixDAO implements MatrixDAO{
    private JdbcTemplate jdbcTemplate;
 
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Matrix findByID(int matrixId) {
        String sqlForID = "SELECT * FROM matrix WHERE matrix_id = ?";
        Matrix matrix = (Matrix)jdbcTemplate.queryForObject(
		sqlForID, new Object[] { matrixId }, 
		new MatrixRowMapper());
        return matrix;
    }
    
    @Override
    public String findMatrixNameById(int matrixId){return findByID(matrixId).getMatrixName();}
    
    @Override
    public int getMatrixLength(int matrixId){return findByID(matrixId).getMatrixLength();}

    @Override
    public void insertRecord(Matrix insertObject) {
        String insertMatrix = "INSERT INTO matrix (user_id, matrix_name, matrix_length) VALUES (?, ?, ?)";
        jdbcTemplate.update(insertMatrix, new Object[] { insertObject.getUserId(),
            insertObject.getMatrixName(),insertObject.getMatrixLength()
	});
    }

    @Override
    public int findIdByName(String matrixName) {
        String sqlForName = "SELECT * FROM matrix WHERE matrix_name = ?";
        Matrix matrix = (Matrix)jdbcTemplate.queryForObject(
		sqlForName, new Object[] { matrixName }, 
		new MatrixRowMapper());
        return matrix.getMatrixId();
    }
}