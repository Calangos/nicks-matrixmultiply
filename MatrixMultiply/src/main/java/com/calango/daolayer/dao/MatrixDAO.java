package com.calango.daolayer.dao;

import com.calango.daolayer.model.Matrix;

/**
 *
 * @author Calango
 */
public interface MatrixDAO {
    public Matrix findByID(int id);
    public String findMatrixNameById(int matrixId);
    public int getMatrixLength(int matrix_id);
    public void insertRecord(Matrix inserObject);
    public int findIdByName(String matrixName);
}
