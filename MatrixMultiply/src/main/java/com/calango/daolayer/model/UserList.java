package com.calango.daolayer.model;

/**
 *
 * @author Calango
 */
public class UserList {
    int userID;
    String userLogin;
    String userPassword;
    
    public UserList(int userID, String userLogin, String userPassword){
        this.userID = userID;
        this.userLogin = userLogin;
        this.userPassword = userPassword;
    }

    public int getUserID() {
        return userID;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
}
