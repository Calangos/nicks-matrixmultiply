package com.calango.daolayer.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Calango
 */
@Entity
@Table(name="matrix")
public class Matrix implements Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "matrix_id", nullable = false)   
    int matrixId;
    @Column(name = "user_id")
    int userId;
    @Column(name = "matrix_name")        
    String matrixName;
    @Column(name = "matrix_length")
    int matrixLength;
    
    public Matrix(){}
    
    public Matrix(int matrixId, int userId, String matrixName, int matrixLength){
        this.matrixId = matrixId;
        this.userId = userId;
        this.matrixName = matrixName;
        this.matrixLength = matrixLength;
    }

    public int getMatrixId() {
        return matrixId;
    }

    public int getUserId() {
        return userId;
    }

    public String getMatrixName() {
        return matrixName;
    }

    public int getMatrixLength() {
        return matrixLength;
    }

    public void setMatrixId(int matrixId) {
        this.matrixId = matrixId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setMatrixName(String matrixName) {
        this.matrixName = matrixName;
    }

    public void setMatrixLength(int matrixLength) {
        this.matrixLength = matrixLength;
    }
}
