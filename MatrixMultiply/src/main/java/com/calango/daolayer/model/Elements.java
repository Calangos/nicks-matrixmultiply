package com.calango.daolayer.model;

/**
 *
 * @author Calango
 */
public class Elements {
    int elementId;
    int rowId;
    int columnId;
    int value;
    int matrixId;
    
    public Elements(){}
    
    public Elements(int elementId, int rowID, int columnID, int value, int matrixID){
        this.elementId = elementId;
        this.rowId = rowID;
        this.columnId = columnID;
        this.value = value;
        this.matrixId = matrixID;
    }

    public int getElementId() {
        return elementId;
    }
    
    public int getRowId() {
        return rowId;
    }

    public int getColumnId() {
        return columnId;
    }

    public int getValue() {
        return value;
    }

    public int getMatrixId() {
        return matrixId;
    }

    public void setElementId(int elementId) {
        this.elementId = elementId;
    }

    public void setRowId(int rowId) {
        this.rowId = rowId;
    }

    public void setColumnId(int columnId) {
        this.columnId = columnId;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setMatrixId(int matrixId) {
        this.matrixId = matrixId;
    }  
}
